import nose
import unittest
import TrivialUI
import argparse
from PyQt5.QtWidgets import QApplication, QMainWindow

def suite_to_list(suite):
    children = []
    for test in suite:
        if isinstance(test, nose.suite.ContextSuite):
            children.append([test.context.__name__,
                             None,
                             suite_to_list(test)])
        else:
            children.append([str(test), test, "not run"])
    return children

def get_tests(directory):
    loader = nose.loader.TestLoader()
    return sum((suite_to_list(suite)
                for suite in loader.loadTestsFromDir(directory)),
               [])

class MainWindow(TrivialUI.MainWindow):
    def __init__(self, tests):
        super(MainWindow, self).__init__(
            menus={'&Tests': {'&Run all': self.run_all,
                              'Run &failed': self.run_failed}})

        self.tests = tests

        self.tree_view = TrivialUI.NestedListTreeView(self.tests)
        self.setCentralWidget(self.tree_view.treeView)

    def run_all(self):
        for entry in self.tests:
            if not isinstance(entry[2], list):
                name, test, status = entry
                result = unittest.TestResult()
                test.run(result)
                if result.wasSuccessful():
                    entry[2] = "pass"
                else:
                    entry[2] = "fail"
            else:
                name, click_target, children = entry
                self._run_suite(children)

    def _run_suite(self, suite):
        for entry in suite:
            if not isinstance(entry[2], list):
                name, test, status = entry
                result = unittest.TestResult()
                test.run(result)
                if result.wasSuccessful():
                    entry[2] = "pass"
                else:
                    entry[2] = "fail"
            else:
                name, click_target, children = entry
                self._run_suite(children)

    def run_failed(self):
        print("Not yet supported...")

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument('directory', help='Directory from which to load tests')

if __name__ == '__main__':
    args = argument_parser.parse_args()

    import sys
    app = QApplication(sys.argv)
    tests = get_tests(args.directory)
    main_window = MainWindow(tests)
    main_window.show()
    sys.exit(app.exec_())
